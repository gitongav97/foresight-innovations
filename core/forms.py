from django import forms

Q_CHOICES = [
            ('Commercial & Residential Cleaning', 'Commercial & Residential Cleaning'),
            ('Executive Office Cleaning', 'Executive Office Cleaning'),
            ('Sanitation & Washroom Maintenance', 'Sanitation & Washroom Maintenance'),
            ('Landscaping & Gardening', 'Landscaping & Gardening'),
            ('Carpet & Upholstery Cleaning', 'Carpet & Upholstery Cleaning'),
            ('Office Records Maintenance', 'Office Records Maintenance'),
            ('After Event Clean Up', 'After Event Clean Up'),
            ('Post Construction Cleaning', 'Post Construction Cleaning'),
            ('Other(specify)', 'Other(specify)')
]

class ContactForm(forms.Form):

    name = forms.CharField(label='Name', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Name' }))
    email = forms.EmailField(label='Email', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Email'}))
    phone_number = forms.IntegerField(label='Phone Number', widget=forms.NumberInput(attrs={'class': 'form-control', 'placeholder':'Phone Number'}))
    company = forms.CharField(label='Company', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Company'}))
    subject = forms.CharField(label='Subject', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Subject'}))
    message = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control', 'placeholder':'Message', 'cols':30, 'rows':5}))



class RequestQuoteForm(forms.Form):

    name = forms.CharField(label='Name', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Name' }))
    email = forms.EmailField(label='Email', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Email'}))
    phone_number = forms.IntegerField(label='Phone Number', widget=forms.NumberInput(attrs={'class': 'form-control', 'placeholder':'Phone Number'}))
    company = forms.CharField(label='Company', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Company'}))
    service = forms.ChoiceField(label='Select', widget=forms.Select(attrs={'class': 'form-control'}), choices=Q_CHOICES)
    area_size = forms.IntegerField(label='area', widget=forms.NumberInput(attrs={'class': 'form-control', 'placeholder':'Estimated Size, SqFt'}))
    other_detail = forms.CharField(label='Info', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'More Details'}))