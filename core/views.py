from django.shortcuts import render, redirect
from .forms import *

from django.core.mail import send_mail
from django.contrib import messages

# Create your views here.


def home_view(request):


    return render(request, 'home.html')



def company_view(request):


    return render(request, 'company.html')



def services_view(request):


    return render(request, 'services.html')



def contact_view(request):

    form = ContactForm(request.POST or None)

    if form.is_valid():

        name = form.cleaned_data.get('name')
        email = form.cleaned_data.get('email')
        phone_number = form.cleaned_data.get('phone_number')
        company = form.cleaned_data.get('company')
        subject = form.cleaned_data.get('subject')
        message = form.cleaned_data.get('message')

        text_content = {
            'Name':'Name: ' + name,
            'Email':'Email: ' + email,
            'Phone Number':'Phone Number: '+ '+254 - '+ str(phone_number),
            'Company':'Company: ' + company,
            'Subject':'Subject: ' + subject,
            'Message':'Message: ' +message
        }

        print(text_content)


        msg = "\n".join(text_content.values())

        subject = 'Contact Request'
        send_mail(subject, msg, 'Contact Request <sales@foresightinnovations.co.ke>', ['gitongav97@gmail.com'])

        messages.success(request, 'Thank you for contacting us. Someone from our team will be in contact with you')


        return redirect('home-view')




    context = {

        'form':form
    }


    return render(request, 'contact.html', context)



def request_quote(request):


    form = RequestQuoteForm(request.POST or None)

    if form.is_valid():

        name = form.cleaned_data.get('name')
        email = form.cleaned_data.get('email')
        phone_number = form.cleaned_data.get('phone_number')
        company = form.cleaned_data.get('company')
        service = form.cleaned_data.get('service')
        area_size = form.cleaned_data.get('area_size')
        other_details = form.cleaned_data.get('other_detail')

        text_content = {

            'Company':'Company: ' + company,
            'Name':'Name: ' + name,
            'Email':'Email: ' + email,
            'Phone Number':'Phone Number: '+ '+254 - '+ str(phone_number),
            'Service': 'Service: ' + service,
            'Area':'Area Size: ' + str(area_size) + 'SqFt',
            'Details': 'More Details: ' + other_details,
        }

        print(text_content)


        msg = "\n".join(text_content.values())

        subject = 'Quote Request'
        send_mail(subject, msg, 'Quote Request <sales@foresightinnovations.co.ke>', ['gitongav97@gmail.com'])

        messages.success(request, 'Your quote request has been received. Someone from our team will be in contact with you')


        return redirect('home-view')




    context = {

        'form':form
    }


    return render(request, 'request_quote.html', context)